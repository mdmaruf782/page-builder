<?php
	
	/**
	 * Dump data for helpeer function
	 *
	 * @param       mixed  	$data    Input string
	 * @param       bool  	$die    Input string
	 * @return      mixed
	 */

	function dumper($data, $die = FALSE)
	{
		echo '<pre>'; print_r($data); echo '</pre>';

		if ($die) {
			die('Hey SQB!, Dumped here...!!!');
		}
	}


	/**
	 * Check LoggedIn or Not
	 *
	 * @return      string
	 */

	function is_logged_in() {

		if ($_SESSION['username'] == NULL &&  $_SESSION['logged_in']!= TRUE) {

			return false;
		}
		else {
			return true;
		}
	}


	/**
	 * Redirect URL
	 *
	 * @param       string  $url    Input URL
	 * @return      string
	 */

	function redirect($url) {
    	return header('Location: '.$url);
    }


    