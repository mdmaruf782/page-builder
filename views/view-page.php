<?php
require_once "../helpers/cms_helper.php";
require_once "../models/page_model.php";
$obj=new page_model;

$get_data=$obj->view();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <link rel="stylesheet" type="text/css" href="plugins/bootstrap-3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="plugins/font-awesome-4.5.0/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="css/examples.css" />
    <script type="text/javascript" src="plugins/jquery-1.11.3/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="examples/plugins/bootstrap-3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
 <nav class="navbar navbar-inverse">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Back <span class="sr-only">(current)</span></a></li>
                    
                    
                </ul>
                
                
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
 <div class="container">
    <?php 
    echo $get_data['content'];
     ?>
 </div>
</body>
</html>