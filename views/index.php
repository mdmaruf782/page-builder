<?php
require_once "../helpers/cms_helper.php";
require_once "../models/page_model.php";
$obj=new page_model;
if (isset($_POST['action'])) {
    $obj->data($_POST['content']);
    $obj->submit();

}
$get_data=$obj->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>KEditor - Kademi Content Editor</title>
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap-3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="plugins/font-awesome-4.5.0/css/font-awesome.min.css" />
    <!-- Start of KEditor styles -->
    <link rel="stylesheet" type="text/css" href="dist/css/keditor-1.1.5.min.css" />
    <link rel="stylesheet" type="text/css" href="dist/css/keditor-components-1.1.5.min.css" />
    <!-- End of KEditor styles -->
    <link rel="stylesheet" type="text/css" href="css/examples.css" />
    <script type="text/javascript" src="plugins/jquery-1.11.3/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="examples/plugins/bootstrap-3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        var bsTooltip = $.fn.tooltip;
        var bsButton = $.fn.button;
    </script>
    <script src="plugins/jquery-ui-1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $.widget.bridge('uibutton', $.ui.button);
        $.widget.bridge('uitooltip', $.ui.tooltip);
        $.fn.tooltip = bsTooltip;
        $.fn.button = bsButton;
    </script>
    <script type="text/javascript" src="plugins/jquery.nicescroll-3.6.6/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="plugins/ckeditor-4.5.6/ckeditor.js"></script>
    <script type="text/javascript" src="plugins/ckeditor-4.5.6/adapters/jquery.js"></script>
    <!-- Start of KEditor scripts -->
    <script type="text/javascript" src="dist/js/keditor-1.1.5.js"></script>
    <script type="text/javascript" src="dist/js/keditor-components-1.1.5.js"></script>
    <!-- End of KEditor scripts -->
</head>
<body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="view-page.php">Page-View <span class="sr-only">(current)</span></a></li>
                    <button class="btn btn-success" id="save" style="margin-top: 10px;">save</button>

                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div id="content-area">
        <?php
        echo $get_data['content'];
        ?>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#content-area').keditor();
            $('#save').click(function(){
                $.ajax({
                    type: 'post',
                    data: {
                        action : "send-content",
                        content: $('#content-area').keditor('getContent')
                    },
                    success: function(data){
                // console.log(data);
                alert("saved");
            },
            error: function(data){
                //  console.log(data);
            }
        });
                console.log($('#content-area').keditor('getContent'));
            });
        });
    </script>
</body>
</html>