<?php

	// Load required core file
	require_once 'config/config.php';
	require_once 'helpers/cms_helper.php';
	// require_once 'libraries/DBE.php';

	// Test dumper() and redirect() helper
	/*
		$arrayName = array('sat' => 'SAT', 'sun' => 'SUN', 'mon' => 'MON', );
		dumper($arrayName, TRUE);
		redirect('dashboard.php');
	*/
	
	// Test DBE class
	/*
		$dbe = new DBE();

		$data_admin = $dbe->select('admin');
		$data_admin = $dbe->select_by_id('1','admin');
		$data_admin = $dbe->select_by_un_pass('sqb','1234');
		$data_admin = $dbe->select_custom("SELECT * FROM admin");
		$data_admin = $dbe->select_row("SELECT * FROM admin WHERE id=2");
		$data_admin = $dbe->insert("INSERT INTO admin(username, password) VALUES('test','1234')");
		$data_admin = $dbe->update("UPDATE admin SET name = 'TEST' WHERE id = 3");
		$data_admin = $dbe->delete("4", "admin");
		dumper($data_admin);
	*/


		$page_url = (isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on') ? "https://" : "http://";
	    
	    $page_url .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];

		dumper($_SERVER, TRUE);