<?php

	/**
	 * DBE Class for manipulating data in database.
	 *
	 * @category    Libraries
	 * @author      Shaquib Mahmud
	 * @link        http://www.sqbctgbd.com
	 */


	// Load requied file
	require_once '../config/db_config.php';
	require_once '../helpers/cms_helper.php';

	class DBE {
		
		public $link;
		public $error;

		function __construct() {
			$this->link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		}

		/**
		 * SELECT ALL DATA
		 *
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public function select($table) {
			
			$query = "SELECT * FROM $table";

			$result = $this->link->query($query);

			if ($result->num_rows > 0) {
				while($row = $result -> fetch_assoc()) {
					$records[] = $row;
				}

				$result -> free();
				return $records;
			}
			else {
				return false;
			}
		}


		/**
		 * Select Data BY ID
		 *
		 * @param       number  $id 	  Input ID
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public function select_by_id($id, $table) {

			$query = "SELECT * FROM $table WHERE id=$id";

			$result = $this->link->query($query);
			
			if ($result->num_rows > 0) {
				return $result->fetch_assoc();
			}
			else {
				return false;
			}
		}

		/**
		 * SELECT ALL DATA from Multiple Table (Joining)
		 *
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public function select_custom($query) {
			
			$result = $this->link->query($query);

			if ($result->num_rows > 0) {
				while($row = $result -> fetch_assoc()) {
					$records[] = $row;
				}

				$result -> free();
				return $records;
			}
			else {
				return false;
			}
		}


		/**
		 * SELECT Single Row 
		 *
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public function select_row($query) {

			$result = $this->link->query($query);
			
			if ($result->num_rows > 0) {
				return $result->fetch_assoc();
			}
			else {
				return false;
			}
		}


		/**
		 * Insert Data
		 *
		 * @param       string  $query 	  Input Query
		 * @return 		bool / last insert ID
		 */

		public function insert($query) {

			// return last insert ID
			if ($this->link->query($query) == TRUE) {
    			$last_insert_id = $this->link->insert_id;
    		}
			
			if (!$last_insert_id) {
				die("Error :(".$this->link->errno.")".$this->link->error);
			}

			return $last_insert_id;
		}

		
		/**
		 * Update Data
		 *
		 * @param       string  $query 	  Input Query
		 * @return 		bool
		 */

		public function update($query) {

			$update_row = $this->link->query($query);

			if (!$update_row) {
				die("Error :(".$this->link->errno.")".$this->link->error);
			}

			return $update_row;
		}


		/**
		 * Delete Data
		 *
		 * @param       number  $id 	  Input ID
		 * @param       string  $table    Input table name
		 * @return 		bool
		 */

		public function delete($id, $table) {

			$query = "DELETE FROM $table WHERE id=$id";

			$delete_row = $this->link->query($query);

			if (!$delete_row) {
				die("Error :(".$this->link->errno.")".$this->link->error);
			}

			return $delete_row;
		}


		/**
		 * Escape Data
		 *
		 * @param       string  $input    Input data
		 * @return 		bool
		 */

		public function escape($input) {
			$str = $this->link->real_escape_string($input);
			$escapeStr = htmlspecialchars(htmlentities($str));
			return $escapeStr;
		}

		
		
}		