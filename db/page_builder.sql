-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2019 at 07:10 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `page_builder`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `admin_id`, `content`) VALUES
(3, 1, '<section><div class=\"row\">\n        <div class=\"col-sm-12\" data-type=\"container-content\">\n        <section data-type=\"component-text\"><div class=\"page-header\">\n<h1 style=\"margin-bottom: 30px; font-size: 50px;\"><b class=\"text-uppercase\">Cras justo odio</b> <small>Donec id elit non mi</small></h1>\n\n<p class=\"lead\"><em>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</em></p>\n</div>\n</section><section><div class=\"row\">\n        <div class=\"col-sm-12\" data-type=\"container-content\">\n        <section data-type=\"component-text\"><div class=\"row\">\n<div class=\"col-md-4 text-center\"><a href=\"https://cdn.pixabay.com/photo/2016/09/21/04/46/barley-field-1684052_1280.jpg\"><img class=\"img-circle img-responsive\" src=\"https://cdn.pixabay.com/photo/2016/09/21/04/46/barley-field-1684052_1280.jpg\" style=\"display: inline-block; height: 300px; width: 450px;\"></a>\n<h3>Lorem ipsum</h3>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel, alias, temporibus? Vero natus modi ipsa debitis, accusamus accusantium cum quam. Saepe atque quisquam pariatur voluptatem expedita nesciunt reprehenderit et vitae.</p>\n</div>\n\n<div class=\"col-md-4 text-center\"><img class=\"img-circle img-responsive\" src=\"snippets/default/img/wellington_newzealand_squared.jpg\" style=\"display: inline-block;\">\n<h3>Lorem ipsum</h3>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, aut, earum. Quod, debitis, delectus. Maxime eius ipsam sit dolorum perspiciatis obcaecati consectetur, explicabo reprehenderit repellat tempore veniam eos ducimus! Dignissimos.</p>\n</div>\n\n<div class=\"col-md-4 text-center\"><img class=\"img-circle img-responsive\" src=\"snippets/default/img/yenbai_vietnam_squared.jpg\" style=\"display: inline-block;\">\n<h3>Lorem ipsum</h3>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil voluptatibus dicta corrupti aliquam, natus voluptatem pariatur quidem nostrum nisi corporis id ratione exercitationem et recusandae incidunt assumenda soluta qui odit.</p>\n</div>\n</div>\n</section></div>\n    </div></section><section><div class=\"row\">\n        <div class=\"col-sm-6\" data-type=\"container-content\">\n        <section data-type=\"component-text\"><div class=\"media\">\n<div class=\"media-left\"><a href=\"#\"><img class=\"media-object\" height=\"\" src=\"snippets/default/img/yenbai_vietnam.jpg\" width=\"150\"> </a></div>\n\n<div class=\"media-body\">\n<h4 class=\"media-heading\">Media heading</h4>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos minus hic praesentium, nihil nemo, optio delectus explicabo at beatae. Ullam itaque, officiis maxime quibusdam impedit vero?</p>\n</div>\n</div>\n</section><section data-type=\"component-audio\"></section><section data-type=\"component-text\"><div class=\"media\">\n<div class=\"media-left\"><a href=\"#\"><img class=\"media-object\" height=\"\" src=\"snippets/default/img/yenbai_vietnam.jpg\" width=\"150\"> </a></div>\n\n<div class=\"media-body\">\n<h4 class=\"media-heading\">Media heading</h4>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos minus hic praesentium, nihil nemo, optio delectus explicabo at beatae. Ullam itaque, officiis maxime quibusdam impedit vero?</p>\n</div>\n</div>\n</section><section data-type=\"component-text\"><div class=\"media\">\n<div class=\"media-left\"><a href=\"#\"><img class=\"media-object\" height=\"\" src=\"snippets/default/img/yenbai_vietnam.jpg\" width=\"150\"> </a></div>\n\n<div class=\"media-body\">\n<h4 class=\"media-heading\">Media heading</h4>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos minus hic praesentium, nihil nemo, optio delectus explicabo at beatae. Ullam itaque, officiis maxime quibusdam impedit vero?</p>\n</div>\n</div>\n</section><section data-type=\"component-audio\"></section></div>\n        <div class=\"col-sm-6\" data-type=\"container-content\">\n        <section data-type=\"component-googlemap\"><div class=\"googlemap-wrapper\">\n        <div class=\"embed-responsive embed-responsive-16by9\">\n            <iframe class=\"embed-responsive-item\" src=\"https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14897.682811563638!2d105.82315895!3d21.0158462!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1456466192755\"></iframe>\n        </div>\n    </div></section></div>\n    </div></section><section><div class=\"row\">\n        <div class=\"col-sm-12\" data-type=\"container-content\">\n        <section data-type=\"component-photo\"><div class=\"photo-panel\">\n        <img src=\"https://cdn.pixabay.com/photo/2017/08/02/13/09/confetti-2571539_1280.jpg\" width=\"100%\" height=\"\" style=\"display: inline-block;\">\n    </div></section></div>\n    </div></section></div>\n    </div></section>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
